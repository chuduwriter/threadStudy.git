package com.yhx.studyDemo;

import org.apache.commons.lang.StringUtils;

import java.util.Optional;
import java.util.stream.Stream;

public class OptionalTest {


    public static void main(String[] args) {
       // Optional o=Optional.of(null);
        //System.out.println(o);

        Optional o1=Optional.ofNullable("zhangsan");
        System.out.println(o1);
        Optional o2=Optional.ofNullable("zhangsan1");
        System.out.println(o2);
        System.out.println(o1.equals(o2));

        if(o1.isPresent()){
            System.out.println("empty");
        }else{
            String str=(String)o1.get();
            System.out.println(str);
        }


        Optional<Student> o3=Optional.of(new Student("zhangsan","12"));
        Optional o33=o3.filter((student)->student.getName().length()>3);
        Optional o34=o3.map((student -> StringUtils.isBlank("")?new Student("zhangsa","88"):setName("4545",student)));
        Optional o35=o3.flatMap(student -> Optional.ofNullable(new Student("lisi","9090")));
        if(!o35.isPresent()){
            System.out.println("empty");
            return;
        }
        Optional<Student> o36=Optional.ofNullable(new Student());
        Student s=(Student) o36.orElse(new Student());
        Student ss=(Student)o36.orElseGet(()->setName("45656",new Student()));
       // o36.isPresent(ww->{ ww.});
        System.out.println(ss.getName());
        System.out.println(s.getName());
        Student student=(Student)o35.get();
        System.out.println(student.getName());


        Stream<String> names = Stream.of("Lamurudu", "Okanbi", "Oduduwa");

        Optional<String> startswl = names.filter(name -> name.startsWith("L")).findFirst();

        //判断是否不为null
        if(startswl.isPresent()){
            System.out.println(startswl.get());
        }

        //if值为null：打印“null”；if值不为null：打印原值
        System.out.println(startswl.orElse("null"));

        //if值不为null，执行Lambda表达式
        startswl.ifPresent(name -> {
            String sss = name.toUpperCase();
            System.out.println("jfaojfoejfoei"+sss);
        });
    }

    private static Student setName(String name ,Student student){
        student.setName(name);
        return student;
    }

    static class Student{
        private String name="1000";

        private String age="1000";

        public Student() {
        }

        public Student(String name, String age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }
    }
}
