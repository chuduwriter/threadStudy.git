package com.yhx.studyDemo.department;

import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

public class TreeNode implements Serializable {
    private long parentId;
    private long id;
    protected String departMentName;
    protected List<TreeNode> childList;

    public TreeNode() {
        initChildList();
    }

    public boolean isLeaf() {
        if (childList == null) {
            return true;
        } else {
            if (childList.isEmpty()) {
                return true;
            } else {
                return false;
            }
        }
    }

    /* 插入一个child节点到当前节点中 */
    public void addChildNode(TreeNode treeNode) {
        initChildList();
        childList.add(treeNode);
    }

    public void initChildList() {
        if (childList == null){
            childList = new ArrayList<TreeNode>();
        }
    }


    /* 返回当前节点的孩子集合 */
    public List<TreeNode> getChildList() {
        return childList;
    }



    /* 遍历一棵树，层次遍历 */
    public void traverse() {
        if (id < 0)
            return;
        if (childList == null || childList.isEmpty())
            return;
        int childNumber = childList.size();
        for (int i = 0; i < childNumber; i++) {
            TreeNode child = childList.get(i);
            child.traverse();
        }
    }


    public void setChildList(List<TreeNode> childList) {
        this.childList = childList;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDepartMentName() {
        return departMentName;
    }

    public void setDepartMentName(String departMentName) {
        this.departMentName = departMentName;
    }


}