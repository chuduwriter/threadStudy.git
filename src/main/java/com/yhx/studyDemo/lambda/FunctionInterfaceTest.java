package com.yhx.studyDemo.lambda;

public class FunctionInterfaceTest {


    public void testLambda() {

        func(new FunctionInterface() {
            @Override
            public String test(Object param) {
                System.out.println(""+param);
                return "param";
            }
        });
        func((x) -> "true");

    }


    private void func(FunctionInterface<String> functionInterface) {
        int x=1;
        functionInterface.test(1+"");
    }

    public static void main(String[] args) {
        FunctionInterfaceTest f=new FunctionInterfaceTest();
        f.testLambda();
    }
}
