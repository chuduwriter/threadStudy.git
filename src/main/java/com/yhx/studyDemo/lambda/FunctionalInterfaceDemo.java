package com.yhx.studyDemo.lambda;


@FunctionalInterface
public interface FunctionalInterfaceDemo {
    boolean test(Integer x);
}
