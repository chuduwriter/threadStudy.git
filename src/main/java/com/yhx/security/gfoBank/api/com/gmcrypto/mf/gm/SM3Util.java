package com.yhx.security.gfoBank.api.com.gmcrypto.mf.gm;

import java.util.Arrays;

import org.bouncycastle.crypto.digests.SM3Digest;

/**
 * ����sm3 hashֵ
 * <p>��������˵�����ӿڽ��յĶ���ԭʼ�Ķ��������ݣ���hex����base64��������ݣ���ؽ���֮���ٴ�����
 * @author liangruxing-yfzx
 *
 */
public class SM3Util {

	
	/**
	 * ����hashֵ��������������ʱʹ�ã���������Ӧʹ��ԭ���ӿڣ��ֶμ���sm3ֵ
	 * @param srcData ������hashֵ������
	 * @return
	 */
	public static byte[] hash(byte[] srcData) {
        SM3Digest digest = new SM3Digest();
        digest.update(srcData, 0, srcData.length);
        byte[] hash = new byte[digest.getDigestSize()];
        digest.doFinal(hash, 0);
        return hash;
    }

    /**
     * У��sm3ֵ��������������ʱʹ�ã���������Ӧʹ��ԭ���ӿڣ��ֶμ���sm3ֵ��Ȼ��У��
     * @param srcData ����֤������
     * @param sm3Hash ����֤��hashֵ
     * @return
     */
    public static boolean verifyHash(byte[] srcData, byte[] sm3Hash) {
        byte[] newHash = hash(srcData);
        if (Arrays.equals(newHash, sm3Hash)) {
            return true;
        } else {
            return false;
        }
    }
    
}
