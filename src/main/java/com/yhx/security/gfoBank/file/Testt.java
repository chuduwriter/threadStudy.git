package com.yhx.security.gfoBank.file;

import net.sourceforge.pinyin4j.PinyinHelper;

import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;

/**
 * @author: mishuai
 * @date: 2020/12/2 10:51
 * @description:
 */
public class Testt {
    public static void main(String[] args) {
        //sortByFirstCha();
        System.out.println(getPinYinHeadChar("宿迁里"));
        String zm=getPinYinHeadChar("宿迁里");
        Character first=zm.charAt(0);
        String firstStr=first.toString();
        System.out.println(firstStr);
    }

    /**
     * 得到中文首字母
     *
     * @param str
     * @return
     */
    public static String getPinYinHeadChar(String str) {
        int tmp= 2;
        if(str.length() < 2){
            tmp = str.length();
        }
        String convert = "";
        for (int j = 0; j < tmp; j++) {
            char word = str.charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert += pinyinArray[0].charAt(0);
            } else {
                convert += word;
            }
        }
        return convert.toUpperCase();
    }

    private static void sortByFirstCha() {
        // Collator 类是用来执行区分语言环境的 String 比较的，这里选择使用CHINA
        Comparator cmp = Collator.getInstance(java.util.Locale.CHINA);
        String[] arr = {"张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五",
                "啊六","张三", "李四", "王五", "啊六","张三",
                "李四", "王五", "啊六","张三", "李四", "王五", "啊六","张三", "李四", "王五", "啊六",};
        // 使根据指定比较器产生的顺序对指定对象数组进行排序。
        Arrays.sort(arr, cmp);
        for (int i = 0; i < arr.length; i++)
            System.out.println(arr[i]);
        System.out.println();
    }
}
