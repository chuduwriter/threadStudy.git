package com.yhx.security.guangfa;

import java.io.*;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipFileUnpack {
	
	public static void unpacker(String filePath,String unzipfilePath) {
		
		ZipEntry zipEntry = null;
		BufferedOutputStream bos = null;
		BufferedInputStream bis = null;
		int count = 0;
		byte[] buffer = new byte[1024];
		//���ѹ�����ļ�
		try {
			ZipFile zip = new ZipFile(filePath);
			Enumeration<ZipEntry> entrys = (Enumeration<ZipEntry>) zip.entries();
			while(entrys.hasMoreElements()) {
				zipEntry = entrys.nextElement();
				
				String entryFilePath = unzipfilePath + "/" + zipEntry.getName(); 
				File unfile = new File(entryFilePath);
				
				bos = new BufferedOutputStream(new FileOutputStream(unfile));
				bis = new BufferedInputStream(zip.getInputStream(zipEntry));
				
				while((count = bis.read(buffer, 0, 1024))!=-1) {
					bos.write(buffer, 0, count);
				}
				bos.flush();
				bos.close();
				bis.close();
				
			}			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if(bis != null) {
					bis.close();
				}
				if(bis != null) {
					bos.flush();
					bos.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
