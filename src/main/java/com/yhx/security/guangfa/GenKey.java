package com.yhx.security.guangfa;

import cfca.sadk.cgb.toolkit.SM2Toolkit;
import cfca.sadk.system.FileHelper;

import java.io.File;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;


public class GenKey {

	public static void main(String[] args) throws Exception {
		String keyName="100000";
		KeyGen(keyName);
	}


	private static void KeyGen(String merName) throws Exception {
		SM2Toolkit tool = new SM2Toolkit();
		KeyPair key = tool.SM2GenerateKeyPair();
		PrivateKey prik1 = key.getPrivate();
		PublicKey pubk = key.getPublic();


		File dir = new File("cert/" + merName);
		if (!dir.exists()) {
			System.out.println("是否生成目录" + dir.mkdirs());
		}

		FileHelper.write("cert/" + merName + "/" + merName + ".pvk",
				prik1.getEncoded());
		FileHelper.write("cert/" + merName + "/" + merName + ".puk",
				pubk.getEncoded());
		System.out.println("密钥生成成功" + merName);
	}
}
