package com.yhx.security.guangfa;
import cfca.sadk.cgb.toolkit.SM4Toolkit;

public class SM4FileUtil {
	
	/**
	 * 解密文件
	 * @param key
	 * @param inFile
	 * @param outFile
	 * @return
	 * @throws Exception 
	 */
	public static boolean SM4DecryptFile(String key,final String inFile, final String outFile) throws Exception{
		SM4Toolkit toolkit=new SM4Toolkit();
		boolean result=false;
		toolkit.SM4Init(key.getBytes(), key.getBytes());
		result=toolkit.SM4DecryptFile(inFile, outFile);
		return result;
	}
	
	/**
	 * 加密文件
	 * @param key
	 * @param inFile
	 * @param outFile
	 * @return
	 */
	public static void SM4EncryptFile(String key,final String inFile, final String outFile){
		SM4Toolkit toolkit=new SM4Toolkit();
		boolean result=false;
		try {
			toolkit.SM4Init(key.getBytes(), key.getBytes());
			result=toolkit.SM4EncryptFile(inFile, outFile);
			if(result==false){
				throw new Exception("文件加密失败");
			}
		} catch (Exception e) {
		}
		
	}
}