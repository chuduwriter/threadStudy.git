/*

package com.yhx.security.demo;

import java.nio.charset.Charset;
import java.security.DigestException;


import com.yhx.security.demo.parameter.EncryptParameter;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.Base64Utils;



public class SecurityDemo
{
	*/
/**
	 * demo中的密钥为测试数据
	 *//*

	//私钥加密使用
	private static String rsaPrivateKeyBank = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMf6T1VW0D/qcfVgps2uoLIrrp8e1RHgEGd/BDIkg/lHw8HQ9o841+ABaB5jfeR4AMx1XaN1kHrtl01bh2t9qwGWZxdEKW9S9I0iIiq3VWGVbmjzzQBQyOzFva7J5GYkaDDJBFcXL/w8N6VelPx5dVMDdnfi5nlLmIgKFtyRxyoXAgMBAAECgYEAn57NqimESjBBTAIcwnpL7Etvs7Txj7YQgfvhhoCdEuHUgCTiWQmfTj7xM6hoeW/L+2cHGg0iCO0SYv3XBtalIlLTHCvxhFF7WXBtheHB0iHUHh2O+tzu0kQTzy7bQjMMgE0XWhj+BUflFLZTpdPmA/mWeeUQafRbWR8YYXRpi7ECQQDqcOI1KKKDnR4bwCNyBtwNNicf71pIzKN7MndegMtvCjvstYNPiLcFftcA71WGaq3LielsKOt/llNVuyQHZI7vAkEA2l4bk4dq6giz1n4HfVUedx/cnY3KEdLW8+dJjXyqTbP0I2bnZIEXqaByWHU3pZaKV/Yro/jvbfP6LCL9OKYXWQJABYUI3mWJhf3CArdAXmPo9Gqs+ySAhKxEwRbMFq6Dm6arCISxhcLbREbbOwtngRWr59nkZAaGnOdmKOOSEpZ8gwJAfG0JK4wK+KXSy1JutBSvUlN+YuwrCvLndn0ApTU22SXD4/Q7v0HrmMUKwqqv9ZOM6sgjX8B1tWJC16lsPj4tqQJBAKpq3eYA6/n2ffWmd1oaqKSmBOI5HNZEnd6Y8upeUPagI1ijSyJhhOfdiFP4ZOzkxxRzAvm3cznMpF7eRvL2M7Y=";
    //公钥和aeskey（24位）双方交换
	private static String jdRsaPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDH+k9VVtA/6nH1YKbNrqCyK66fHtUR4BBnfwQyJIP5R8PB0PaPONfgAWgeY33keADMdV2jdZB67ZdNW4drfasBlmcXRClvUvSNIiIqt1VhlW5o880AUMjsxb2uyeRmJGgwyQRXFy/8PDelXpT8eXVTA3Z34uZ5S5iIChbckccqFwIDAQAB";
	private static String aesKey = "j2oRn7+2sLqfnLjYrvtLCg==";

	*/
/**
	 * 加密方法   1.使用非对称加密方式对内容进行摘要签名使用MD5方式，非对称填充方式pkcs#8,长度1024位 对参数和返回值进行加密（双方交换公钥）   2.对传输内容进行对称加密,双方交换加密的aeskey
	 * @param data 请求参数，明文
	 * @return 返回密文
	 * @throws Exception
	 *//*

	public static String encrypt( String data , String aesKey, String rsaPrivateKey) throws Exception
	{
		EncryptParameter encryptParameter = new EncryptParameter();
		String sign = Base64Utils.encodeToString( RSAUtils11.encryptByPrivateKey(
				DigestUtils.md5Hex( data ).getBytes( Charset.forName( "UTF-8" ) ) , rsaPrivateKey ) );
		String encryptionData = Base64Utils.encodeToString( com.jdpay.hermes.utils.AESUtil.encrypt( data , aesKey ) );
		encryptParameter.setSign( sign );
		encryptParameter.setData( encryptionData );
		return new GsonBuilder().disableHtmlEscaping().create().toJson( encryptParameter );
	}

	*/
/**
	 * 解密方法 1.使用非对称解密方式对传输密文进行公钥解密得到签名   2.对传输内容进行对称aeskey解密  3.使用同样的签名方式对得到的明文内容进行签名与1中的解密签名做对比，判断是否有篡改
	 * @param encryptData 返回参数，密文
	 * @return 返回明文
	 *//*

	public static String decrypt( String encryptData, String aesKey, String rsaPublicKey ) throws Exception
	{
		EncryptResponse encrypResult = new GsonBuilder().disableHtmlEscaping().create().fromJson( encryptData , EncryptResponse.class );
		String digestInfo = new String( RSAUtils11.decryptByPublicKey( Base64Utils.decodeFromString( encrypResult.getSign() )
				, rsaPublicKey ) , Charset.forName( "UTF-8" ) );
		String jsonData = AESUtil.decrypt(
				Base64Utils.decodeFromString( encrypResult.getData() ) , aesKey );
		String currentDigestInfo = DigestUtils.md5Hex( jsonData.getBytes( Charset.forName( "UTF-8" ) ) );
		if( digestInfo.equals( currentDigestInfo ) ){
			return jsonData;
		}
		else{
			throw new DigestException( "摘要信息校验失败" );
		}

	}

	public static void main( String[] args ) throws Exception
	{
		//请求参数加密
		ApplyRequest applyRequest = new ApplyRequest("requestNo", "mobile","type","biz");
		String request = new GsonBuilder().disableHtmlEscaping().create().toJson( applyRequest );
		System.out.println( "请求参数 明文 为：\n" + request );
		String encryptRequest = encrypt( request,aesKey,rsaPrivateKeyBank );
		System.out.println( "请求参数 密文 为：\n" + encryptRequest );




		//返回结果解密 本demo对请求密文直接用公钥和aeskey解密验证签名和获取传输内容
		String encryptResponse = encryptRequest;
		System.out.println( "响应参数 密文 为：\n" + encryptResponse );
		String response = decrypt( encryptResponse,aesKey,jdRsaPublicKey );
		System.out.println( "响应参数 明文 为：\n" + encryptResponse );
		
	}
}
*/
