
package com.yhx.security.demo.parameter;


public class EncryptParameter
{
	private String data;
	
	private String sign;

	public String getData()
	{
		return data;
	}

	public void setData( String data )
	{
		this.data = data;
	}

	public String getSign()
	{
		return sign;
	}

	public void setSign( String sign )
	{
		this.sign = sign;
	}
}
