package com.yhx.security.demo.parameter;



import java.util.Date;

/**
 * @version 1.0
 * @Author zhangxiaoli5
 * @Description 发奖结果回调入参
 * @Date 2019/8/9 16:52
 **/
public class GivePrizeNotifyRequest {

    private String requestNo;
    private String mobile;
    private boolean givePrizeSuccess;
    private Date givePrizeTime;
    private String biz;

    public GivePrizeNotifyRequest() {
    }

    public GivePrizeNotifyRequest(String requestNo, String mobile, boolean givePrizeSuccess, Date givePrizeTime, String biz) {
        this.requestNo = requestNo;
        this.mobile = mobile;
        this.givePrizeSuccess = givePrizeSuccess;
        this.givePrizeTime = givePrizeTime;
        this.biz = biz;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public boolean isGivePrizeSuccess() {
        return givePrizeSuccess;
    }

    public void setGivePrizeSuccess(boolean givePrizeSuccess) {
        this.givePrizeSuccess = givePrizeSuccess;
    }

    public Date getGivePrizeTime() {
        return givePrizeTime;
    }

    public void setGivePrizeTime(Date givePrizeTime) {
        this.givePrizeTime = givePrizeTime;
    }

    public String getBiz() {
        return biz;
    }

    public void setBiz(String biz) {
        this.biz = biz;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GivePrizeNotifyRequest{");
        sb.append("requestNo='").append(requestNo).append('\'');
        sb.append(", mobile='").append(mobile).append('\'');
        sb.append(", givePrizeSuccess=").append(givePrizeSuccess);
        sb.append(", givePrizeTime=").append(givePrizeTime);
        sb.append(", biz='").append(biz).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
