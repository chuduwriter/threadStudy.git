package com.yhx.security.demo.parameter;

public class Response<T> {

    private T data;
    private boolean success;
    private String code;
    private String msg;

    public Response() {
    }

    public Response(T data, boolean success, String code, String msg) {
        this.data = data;
        this.success = success;
        this.code = code;
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "{ \"data\":" + data
                + ", \"success\":" + success
                + ", \"code\":\"" + code + "\""
                + ", \"msg\":\"" + msg + "\""
                + "}";
    }
}
