/*
package com.yhx.security.icbcGuomi.sign;

import com.yhx.security.icbcGuomi.Util;
import org.bouncycastle.util.encoders.Base64;

public class SignTest {

    public static void main(String[] args) throws Exception{
        String plainText = "message digest";
        byte[] sourceData = plainText.getBytes();

        // 国密规范测试私钥
        String prik = "3945208f7b2144b13f36e38ac6d39f95889393692860b51a42fb81ef4df7c5b8";
        String prikS = new String(Base64.encode(Util.hexToByte(prik)));
        System.out.println("prikS: " + prikS);
        System.out.println("");

        String userId = "1234567812345678";

        System.out.println("ID: " + Util.getHexString(userId.getBytes()));
        System.out.println("");

        System.out.println("签名: ");
        byte[] c = SM2Util.sign(userId.getBytes(), Base64.decode(prikS.getBytes()), sourceData);
        System.out.println("sign: " + Util.getHexString(c));
        System.out.println("");

        // 国密规范测试公钥
        String pubk = "040AE4C7798AA0F119471BEE11825BE46202BB79E2A5844495E97C04FF4DF2548A7C0240F88F1CD4E16352A73C17B7F16F07353E53A176D684A9FE0C6BB798E857";
        pubk=  "04"+"09f9df311e5421a150dd7d161e4bc5c672179fad1833fc076bb08ff356f35020"+"ccea490ce26775a52dc6ea718cc1aa600aed05fbf35e084a6632f6072da9ad13";

        String pubkS = new String(Base64.encode(Util.hexToByte(pubk)));
        System.out.println("pubkS: " + pubkS);
        System.out.println("");


        System.out.println("验签 ");
        boolean vs = SM2Util.verifySign(userId.getBytes(), Base64.decode(pubkS.getBytes()), sourceData, c);
        System.out.println("验签结果： " + vs);
        System.out.println("");
    }
}
*/
