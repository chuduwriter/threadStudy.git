package com.yhx.leetcode.java;

import com.alibaba.druid.sql.visitor.functions.Char;

/**
 * @Author: mishuai
 * @Date: 2020/7/1 1:40 上午
 */
public class LeetCode6 {


    public static void main(String[] args) {
        System.out.println(convert1("LEETCODEISHIRING",4));
    }

    public static String convert(String s, int numRows){

        if(s == null || s.length() == 0|| numRows == 1) return s;
        StringBuilder sb[]=new StringBuilder[numRows];
        for(int i= 0;i<numRows ; i++ ) sb[i]=new StringBuilder();
        int index=0;
        int dir=1;
        for(char cc:s.toCharArray()){
            sb[index].append(cc);
            index +=dir;
            if(index == 0 || index == numRows -1) dir= -dir;
        }
        StringBuilder result=new StringBuilder();
        for(int j =0 ;j<numRows ; j++) result.append(sb[j]);
        return result.toString();
    }

    public static String convert1(String s, int numRows){
        if(s == null || s.length() == 0|| numRows == 1) return s;
        int n=2*numRows-2;
        StringBuilder sb[]=new StringBuilder[numRows];
        for(int i= 0;i<numRows ; i++ ) sb[i]=new StringBuilder();
        for(int i = 0;i<s.length();i++){
            int temp=i%n;
            int t=temp<numRows? temp:n-temp;
            sb[t].append(s.toCharArray()[i]);
        }
        StringBuilder result=new StringBuilder();
        for(int j =0 ;j<numRows ; j++) result.append(sb[j]);
        return result.toString();
    }

    //LDREOEIIECIHNTSG
}
