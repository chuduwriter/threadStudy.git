package com.yhx.leetcode.java;

import java.util.HashSet;
import java.util.Set;

/**
 * 3. 无重复字符的最长子串
 * @Author: mishuai
 * @Date: 2020/6/21 11:50 下午
 */
public class LeetCode3 {

/*    3. 无重复字符的最长子串

    给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。

    示例 1:

    输入: "abcabcbb"
    输出: 3
    解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。

    示例 2:

    输入: "bbbbb"
    输出: 1
    解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。

    示例 3:

    输入: "pwwkew"
    输出: 3
    解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
    请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。*/



    //第一次做想错了
        public static int errolengthOfLongestSubstring(String s) {
            char c[]=s.toCharArray();
            int maxLenth=0;
            int temp=0;
            if(s.isEmpty()){
                return 0;
            }
            if(s.length() == 1){
                return 1;
            }

            HashSet<String> hs = new HashSet();
            for(int i=0;i<c.length;i++){
                String sss=String.valueOf(c[i]);
                if(hs.add(sss)){
                    temp ++;
                    continue;
                }
                i --;
                if(temp > maxLenth){
                    maxLenth=temp;
                }
                temp=0;
                hs.clear();

            }
            if(temp > maxLenth){
                maxLenth=temp;
            }
            return maxLenth;
        }

        //leetcode优解
    public static int lengthOfLongestSubstring1(String s) {

        // 哈希集合，记录每个字符是否出现过
        Set<Character> occ = new HashSet<Character>();
        int n = s.length();
        // 右指针，初始值为 -1，相当于我们在字符串的左边界的左侧，还没有开始移动
        int rk = -1, ans = 0;
        for (int i = 0; i < n; ++i) {
            if (i != 0) {
                // 左指针向右移动一格，移除一个字符
                occ.remove(s.charAt(i - 1));
            }
            while (rk + 1 < n && !occ.contains(s.charAt(rk + 1))) {
                // 不断地移动右指针
                occ.add(s.charAt(rk + 1));
                ++rk;
            }
            // 第 i 到 rk 个字符是一个极长的无重复字符子串
            ans = Math.max(ans, rk - i + 1);
        }
        return ans;

    }


    //模仿做的
    public static int lengthOfLongestSubstring2(String s) {
        char c[]=s.toCharArray();
        if(s.isEmpty()){
            return 0;
        }
        if(s.length() ==  1){
            return 1;
        }
        HashSet<Character> ha=new HashSet<>();
        int maxLenth=0,temp=-1,st=0;
        for(int i =0 ;i <c.length;i++){
            if(i !=0){
                ha.remove(c[i -1]);
            }
            while (temp + 1 < c.length &&!ha.contains(c[temp+1])){
                ha.add(c[temp+1]);
                temp ++;
            }
            maxLenth=Math.max(temp -i +1,maxLenth);

        }
        return maxLenth;
    }


    public static void main(String[] args) {
            String s="pwwkew";
        System.out.println(lengthOfLongestSubstring2(s));
    }

    /**
     * @Author: mishuai
     * @Date: 2020/7/2 1:42 下午
     */
    public static class leetcode8 {
    }
}
