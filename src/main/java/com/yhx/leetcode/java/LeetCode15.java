package com.yhx.leetcode.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: mishuai
 * @Date: 2020/7/6 4:16 下午
 */
public class LeetCode15 {

    public static void main(String[] args) {
        int nums[]={3,0,-2,-1,1,2};
        System.out.println(threeSum(nums));
    }
    public static List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        int k = nums.length - 1;
        List<List<Integer>> list = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            if(i>0 && nums[i] == nums[i - 1]){
                continue;
            }
            int firstValue = -nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                if (j>i+1 && nums[j] == nums[j - 1]) {
                    continue;
                }
                if (j < k && nums[j] + nums[k] > firstValue) {
                    k--;
                }
                if (j == k) {
                    break;
                }
                if (nums[j] + nums[k] == firstValue) {
                    List<Integer> listChildren = new ArrayList<Integer>();
                    listChildren.add(nums[i]);
                    listChildren.add(nums[j]);
                    listChildren.add(nums[k]);
                    list.add(listChildren);
                }
                }
            }


        return list;
    }
}
