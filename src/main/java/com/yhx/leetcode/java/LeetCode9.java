package com.yhx.leetcode.java;

/**
 * @Author: mishuai
 * @Date: 2020/7/2 2:09 下午
 */
public class LeetCode9 {

    public static void main(String[] args) {
        System.out.println(isPalindrome(-1));
    }


    public static boolean isPalindrome(int x) {

        if(x <0){
            return false;
        }

        if(x<10){
            return true;
        }
        String xStr=String.valueOf(x);
        int len=xStr.length();
        int flag=0;
        if(len%2 == 0){
            flag=len/2;
        }else{
            flag=len/2 +1;
        }

        char c[]=xStr.toCharArray();
        for(int i=0;i<=flag;i++){
            if(c[i] != c[len-i-1]){
                return false;
            }
        }
        return true;
    }
}
