package com.yhx.share.thisShare;

import org.springframework.web.servlet.tags.EditorAwareTag;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * https://zhuanlan.zhihu.com/p/41648498
 ：高级加密标准，加密算法标准，速度快，安全级别高；AES是一个使用128为分组块的分组加密算法，分组块和128、192或256位的密钥一起作为输入，
   对4×4的字节数组上进行操作。众所周之AES是种十分高效的算法，尤其在8位架构中，这源于它面向字节的设计。
 */
public class AESUtil {

    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
    //加密
    public static byte[] AES_cbc_encrypt(byte[] srcData,byte[] key,byte[] iv) throws Exception
    {

        //此类以独立于提供者的方式指定密钥它可用于从字节数组构造SecretKey ，而无需通过（基于提供程序的） SecretKeyFactory 。
        //此构造函数不检查给定的字节是否确实指定了指定算法的密钥。 例如，如果算法是DES，则此构造函数不检查key是否为8字节长，。 为了执行这些检查，应该使用特定于算法的密钥规范类（在这种情况下： DESKeySpec ）。
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        //此类提供用于加密和解密的加密密码的功能。 它构成了Java Cryptographic Extension（JCE）框架的核心。
        Cipher cipher = Cipher.getInstance(ALGORITHM);
       /**1、ENCRYPT_MODE，整型值1，加密模式，用于Cipher的初始化。
        2、DECRYPT_MODE，整型值2，解密模式，用于Cipher的初始化。
        3、WRAP_MODE，整型值3，包装密钥模式，用于Cipher的初始化。
        4、UNWRAP_MODE，整型值4，解包装密钥模式，用于Cipher的初始化。
        5、PUBLIC_KEY，整型值1，解包装密钥模式下指定密钥类型为公钥。
        6、PRIVATE_KEY，整型值2，解包装密钥模式下指定密钥类型为私钥。
        7、SECRET_KEY，整型值3，解包装密钥模式下指定密钥类型为密钥，主要用于不是非对称加密的密钥(只有一个密钥，不包含私钥和公钥)。*/
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
       // cipher.update();
        /**
         * 结束单部分加密或者解密操作。
         * 此方法接收需要加密或者解密的完整报文，返回处理结果
         * 此方法正常调用结束之后Cipher会重置为初始化状态。
         */
        byte[] encData = cipher.doFinal(srcData);
        return encData;
    }

    //解密
    public static byte[] AES_cbc_decrypt(byte[] encData,byte[] key,byte[] iv) throws Exception
    {
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv));
        byte[] decbbdt = cipher.doFinal(encData);
        return decbbdt;
    }

    public static void main(String[] args) {


        byte[] key= new byte[16];
        byte[] iv = new byte[16];

        String srcStr = "This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5paddinThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding This is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding g PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TESTThis is java default pkcs5padding PKCS5 TEST";
        System.out.println(srcStr);

        //设置key 全8，iv，全1，这里测试用
        for (int i = 0; i <16; i++) {
            key[i] = 8;
            if (i < 16) {iv[i] = 1;}
        }

     try {


        byte[] encbt = AES_cbc_encrypt(srcStr.getBytes(),key,iv);
        byte[] decbt = AES_cbc_decrypt(encbt,key,iv);
        String decStr = new String(decbt);
        System.out.println(decStr);
        System.out.println(decStr.length());

        if(srcStr.equals(decStr))
        {
            System.out.println("TEST PASS");
        }else
        {
            System.out.println("TEST NO PASS");
        }
     }catch (Exception e){
         System.out.println("erro");
     }

    LocalDateTime localDateTime=LocalDateTime.now().plusSeconds(120);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    String dateTime = localDateTime.format(formatter);
    System.out.println(dateTime);
    Date date = new Date();
    long time = 60*1000;//60秒
    Date afterDate = new Date(System.currentTimeMillis() + time);//60秒后的时间
    Date beforeDate = new Date(date .getTime() - time);
    SimpleDateFormat formater
            = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String afterStr=formater.format(afterDate);
    String beforeDateStr=formater.format(beforeDate);
    String dateStr=formater.format(date);

    System.out.println(afterStr);
    System.out.println(beforeDateStr);
    System.out.println(dateStr);

    }
}
