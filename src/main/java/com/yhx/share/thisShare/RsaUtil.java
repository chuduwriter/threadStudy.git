package com.yhx.share.thisShare;


import org.apache.commons.codec.binary.Base64;
import javax.crypto.Cipher;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

/**
 * RSA算法基于一个十分简单的数论事实：将两个大素数相乘十分容易，但是想要对其乘积进行因式分解却极其困难。那么也就是说要使用RSA算法，
 * 前提是必须有两个大素数才行，那么大素数是怎么生成的呢？考虑到RSA算法的高效性和安全性，怎样快速生成一个大素数呢？
 */
public class RsaUtil {

        private static Map<Integer, String> keyMap = new HashMap<>();  //用于封装随机产生的公钥与私钥
        private static Map<Integer, String> keyMap1 = new HashMap<>();  //用于封装随机产生的公钥与私钥
        private static final String RSA1="RSA/ECB/PKCS1Padding";
        public static void main(String[] args) throws Exception {
            //生成公钥和私钥
            genKeyPair();
            //KeyPair keyPair=genKeyPair1();
            //加密字符串
            String message = "678912345678912345678912345678912345678912345678912345678912345678912345678912345678912345678912345678945678912345678912345678967891234567891234567891234567894567891234567891234567896789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789456789123456789123456789678912345678912345678912345678945678912345678912345678967891234567891234567891234567891234567891234567891234567891234567891234567891234567891234567891234567894567891234567891234567896789123456789123456789123456789456789123456789123456789678912345678912345678912345678912345678912345678912345678912345678912345678912345678912345678912345678945678912345678912345678967891234567891234567891234567894567891234567891234567896789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789123456789456789123456789123456789678912345678912345678912345678945678912345678912345678967891234567891234567891234567891234567891234567891234567891234567891234567891234567891234567891234567894567891234567891234567896789123456789123456789123456789456789123456789123456789";
            System.out.println("publicKey:" + keyMap.get(0));
            System.out.println("privatekey:" + keyMap.get(1));
            String messageEn = encrypt(message,keyMap.get(0),null);
            System.out.println(message + "\tsecurityStr:" + messageEn);
            String messageDe = decrypt(messageEn,keyMap.get(1),null);
            System.out.println("sourceStr:" + messageDe);
        }

        /**
         * 随机生成密钥对
         * 而keyGenerator,KeyPairGenerator,SecretKeyFactory的三种使用方法刚好和这三种加密算法类型对上
         *
         * keyGenerator：秘钥生成器，也就是更具算法类型随机生成一个秘钥，例如HMAC，所以这个大部分用在非可逆的算法中
         *
         * SecretKeyFactory：秘密秘钥工厂，言外之意就是需要根据一个秘密（password）去生成一个秘钥,例如DES，PBE，所以大部分使用在对称加密中
         *
         * KeyPairGenerator:秘钥对生成器，也就是可以生成一对秘钥，也就是公钥和私钥，所以大部分使用在非对称加密中
         * @throws NoSuchAlgorithmException
         */
        public static void genKeyPair() throws NoSuchAlgorithmException, IOException {
            // KeyPairGenerator类用于生成公钥和私钥对，基于RSA算法生成对象
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
            // 初始化密钥对生成器，密钥大小为1024位
            keyPairGen.initialize(4096,new SecureRandom());
            // 生成一个密钥对，保存在keyPair中
            KeyPair keyPair = keyPairGen.generateKeyPair();
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();   // 得到私钥
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();  // 得到公钥
            String publicKeyString = new String(Base64.encodeBase64(publicKey.getEncoded()));
            // 得到私钥字符串
            String privateKeyString = new String(Base64.encodeBase64((privateKey.getEncoded())));
            // 将公钥和私钥保存到Map
            keyMap.put(0,publicKeyString);  //0表示公钥
            keyMap.put(1,privateKeyString);  //1表示私钥
        }

    public static KeyPair genKeyPair1() throws NoSuchAlgorithmException, IOException {
        // KeyPairGenerator类用于生成公钥和私钥对，基于RSA算法生成对象
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
        // 初始化密钥对生成器，密钥大小为1024位
        keyPairGen.initialize(1024,new SecureRandom());
        // 生成一个密钥对，保存在keyPair中
        KeyPair keyPair = keyPairGen.generateKeyPair();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();   // 得到私钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();  // 得到公钥
       /* String publicKeyString = new String(Base64.encodeBase64(publicKey.getEncoded()));
        // 得到私钥字符串
        String privateKeyString = new String(Base64.encodeBase64((privateKey.getEncoded())));
        // 将公钥和私钥保存到Map
        keyMap.put(0,publicKeyString);  //0表示公钥
        keyMap.put(1,privateKeyString);  //1表示私钥*/
        return keyPair;
    }
        /**
         * RSA公钥加密
         *
         * @param str
         *            加密字符串
         * @param publicKey
         *            公钥
         * @return 密文
         * @throws Exception
         *             加密过程中的异常信息
         */
        public static String encrypt( String str, String publicKey,KeyPair keyPair) throws Exception{
            //base64编码的公钥
            byte[] decoded = Base64.decodeBase64(publicKey);
            RSAPublicKey pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
            //RSA加密
            System.out.println(pubKey.getAlgorithm());
            Cipher cipher = Cipher.getInstance(RSA1);
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            String outStr = Base64.encodeBase64String(cipher.doFinal(str.getBytes("UTF-8")));
            return outStr;
        }

        /**
         * RSA私钥解密
         *
         * @param str
         *            加密字符串
         * @param privateKey
         *            私钥
         * @return 铭文
         * @throws Exception
         *             解密过程中的异常信息
         */
        public static String decrypt(String str, String privateKey,KeyPair keyPair) throws Exception{
            //64位解码加密后的字符串
            byte[] inputByte = Base64.decodeBase64(str.getBytes("UTF-8"));
            //base64编码的私钥
            byte[] decoded = Base64.decodeBase64(privateKey);
            RSAPrivateKey priKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded));
            //RSA解密
            System.out.println(priKey.getAlgorithm());
            Cipher cipher = Cipher.getInstance(RSA1);
            cipher.init(Cipher.DECRYPT_MODE, priKey);
            String outStr = new String(cipher.doFinal(inputByte));
            return outStr;
        }




}

