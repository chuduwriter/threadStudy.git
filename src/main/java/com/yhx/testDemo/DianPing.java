package com.yhx.testDemo;

/**
 * @author: mishuai
 * @date: 2020/12/5 14:19
 * @description:
 */
@ExcelSheet(name = "Dianping")
public class DianPing {

    @ExcelField(name = "title")
    private String 	title;
    @ExcelField(name = "url")
    private String url;
    @ExcelField(name = "score")
    private String  score;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"title\":\"")
                .append(title).append('\"');
        sb.append(",\"url\":\"")
                .append(url).append('\"');
        sb.append(",\"score\":\"")
                .append(score).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
