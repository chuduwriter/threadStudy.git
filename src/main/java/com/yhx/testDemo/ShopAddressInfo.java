package com.yhx.testDemo;

/**
 * @author: mishuai
 * @date: 2020/11/13 19:37
 * @description:
 */
@ExcelSheet(name = "KA")
public class ShopAddressInfo {




    @ExcelField(name = "shopId")
    private String 	shopId;
    @ExcelField(name = "shopName")
    private String shopName;
    @ExcelField(name = "shopAddress")
    private String  shopAddress;

    @ExcelField(name = "businessType")
    private String  businessType;
    @ExcelField(name = "status")
    private String  status;
    @ExcelField(name = "belongAlphabet")
    private String belongAlphabet;
    @ExcelField(name = "frontTow")
    private String frontTow;


    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getBelongAlphabet() {
        return belongAlphabet;
    }

    public void setBelongAlphabet(String belongAlphabet) {
        this.belongAlphabet = belongAlphabet;
    }

    public String getFrontTow() {
        return frontTow;
    }

    public void setFrontTow(String frontTow) {
        this.frontTow = frontTow;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"shopId\":\"")
                .append(shopId).append('\"');
        sb.append(",\"shopName\":\"")
                .append(shopName).append('\"');
        sb.append(",\"shopAddress\":\"")
                .append(shopAddress).append('\"');
        sb.append(",\"businessType\":\"")
                .append(businessType).append('\"');
        sb.append(",\"belongAlphabet\":\"")
                .append(belongAlphabet).append('\"');
        sb.append(",\"frontTow\":\"")
                .append(frontTow).append('\"');
        sb.append(",\"status\":\"")
                .append(status).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
