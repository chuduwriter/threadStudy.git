package com.yhx.testDemo;

import java.util.List;

/**
 * @author: mishuai
 * @date: 2020/11/5 15:01
 * @description:
 */
public class Students {
    private String age;

    private String name;

    private Integer ageInt;

    private List<Teacher> teacher;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Teacher> getTeacher() {
        return teacher;
    }

    public void setTeacher(List<Teacher> teacher) {
        this.teacher = teacher;
    }

    public Integer getAgeInt() {
        return ageInt;
    }

    public void setAgeInt(Integer ageInt) {
        this.ageInt = ageInt;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"age\":\"")
                .append(age).append('\"');
        sb.append(",\"name\":\"")
                .append(name).append('\"');
        sb.append(",\"ageInt\":")
                .append(ageInt);
        sb.append(",\"teacher\":")
                .append(teacher);
        sb.append('}');
        return sb.toString();
    }
}
