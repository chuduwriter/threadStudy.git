package com.yhx.testDemo;

/**
 * @author: mishuai
 * @date: 2020/11/5 15:01
 * @description:
 */
public class Teacher {

    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"name\":\"")
                .append(name).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
