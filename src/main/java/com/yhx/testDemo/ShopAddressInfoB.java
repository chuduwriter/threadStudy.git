package com.yhx.testDemo;

/**
 * @author: mishuai
 * @date: 2020/11/13 19:37
 * @description:
 */
@ExcelSheet(name = "KA")
public class ShopAddressInfoB {




    @ExcelField(name = "shopId")
    private String 	shopId;
    @ExcelField(name = "shopName")
    private String shopName;
    @ExcelField(name = "shopAddress")
    private String  shopAddress;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"shopId\":\"")
                .append(shopId).append('\"');
        sb.append(",\"shopName\":\"")
                .append(shopName).append('\"');
        sb.append(",\"shopAddress\":\"")
                .append(shopAddress).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
