package com.yhx.testDemo;

import com.yhx.security.gfoBank.file.DataConvertUtils;
import net.sourceforge.pinyin4j.PinyinHelper;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * @author: mishuai
 * @date: 2020/11/13 19:58
 * @description:
 */
public class ExceltB {

    public static void main(String[] args) {
        List<Object> ll = ExcelImportUtil.importExcel("D:\\我的文档\\1224.xlsx", ShopAddressInfoB.class);
        System.out.println(FastJsonUtils.toJSONString(ll));
        boolean b=false;
        List<ShopAddressInfoB> shopAddressInfos=new ArrayList<>();
        if(ll !=null && ll.size()>0){
            for(Object o:ll) {
                ShopAddressInfoB shopInfoExcelVo = (ShopAddressInfoB)o;
                if(StringUtils.isBlank(shopInfoExcelVo.getShopName())){
                    continue;
                }
                shopAddressInfos.add(shopInfoExcelVo);

            }
            System.out.println(FastJsonUtils.toJSONString(shopAddressInfos));
        }




    }


    /**
     * 得到中文首字母
     *
     * @param str
     * @return
     */
    public static String getPinYinHeadChar(String str) {
        int tmp= 2;
        if(str.length() < 2){
            tmp = str.length();
        }
        String convert = "";
        for (int j = 0; j < tmp; j++) {
            char word = str.charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert += pinyinArray[0].charAt(0);
            } else {
                convert += word;
            }
        }
        return convert.toUpperCase();
    }
}
