package com.yhx.testDemo;

/**
 * @author: mishuai
 * @date: 2021/2/1 15:28
 * @description:
 */
@ExcelSheet(name = "Gh-Ga")
public class Huizong {

    @ExcelField(name = "Seq_1")
    private String Seq_1;
    @ExcelField(name = "name2")
    private String name2;
    /*
    @ExcelField(name = "Seq_2")
    private String Seq_2;Gh-Ga
    @ExcelField(name = "Ka")
    private String Ka;
    @ExcelField(name = "Ks")
    private String Ks;
    @ExcelField(name = "Ka_Ks")
    private String Ka_Ks;
    @ExcelField(name = "EffectiveLen")
    private String EffectiveLen;
    @ExcelField(name = "AverageS_sites")
    private String AverageS_sites;
    @ExcelField(name = "AverageN_sites")
    private String AverageN_sites;
    @ExcelField(name = "cN")
    private String cN;
    @ExcelField(name = "cS")
    private String cS;
    @ExcelField(name = "pN")
    private String pN;
    @ExcelField(name = "pS")
    private String pS;
    @ExcelField(name = "Note")
    private String Note;
*/

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getSeq_1() {
        return Seq_1;
    }

    public void setSeq_1(String seq_1) {
        Seq_1 = seq_1;
    }


}
