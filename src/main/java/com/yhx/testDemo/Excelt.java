package com.yhx.testDemo;

import com.yhx.security.gfoBank.file.ComparatorList;
import com.yhx.security.gfoBank.file.DataConvertUtils;
import net.sourceforge.pinyin4j.PinyinHelper;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * @author: mishuai
 * @date: 2020/11/13 19:58
 * @description:
 */
public class Excelt {

    public static void main(String[] args) {
        List<Object> ll = ExcelImportUtil.importExcel("D:\\我的文档\\a8.xlsx", ShopAddressInfo.class);
        System.out.println(FastJsonUtils.toJSONString(ll));
        boolean b=false;
        List<ShopAddressInfo> shopAddressInfos=new ArrayList<>();
        if(ll !=null && ll.size()>0){
            for(Object o:ll) {
                ShopAddressInfo shopInfoExcelVo = (ShopAddressInfo)o;
                if(StringUtils.isBlank(shopInfoExcelVo.getShopName())){
                    continue;
                }
                String alphabet=getPinYinHeadChar(shopInfoExcelVo.getShopName());
             /*   if(shopInfoExcelVo.getBusinessType().equals("金鹰") || shopInfoExcelVo.getBusinessType().equals("大商百货") || shopInfoExcelVo.getBusinessType().equals("中央商场")){
                    shopInfoExcelVo.setShopName(shopInfoExcelVo.getShopName()+"("+shopInfoExcelVo.getBusinessType()+")");
                }*/
                Character c=alphabet.charAt(0);
                shopInfoExcelVo.setBelongAlphabet(c.toString());
                shopInfoExcelVo.setFrontTow(alphabet);
                shopInfoExcelVo.setStatus("t");
                shopAddressInfos.add(shopInfoExcelVo);
              //  System.out.println("ShopInfoExcelVo="+FastJsonUtils.toJSONNoFeatures(shopInfoExcelVo));
                HashMap<String, Object> hashMap = DataConvertUtils.javaBean2Map(shopInfoExcelVo);
              //  System.out.println("hashMap="+FastJsonUtils.toJSONNoFeatures(hashMap));
            }
        }


       // ComparatorList comparatorList=new ComparatorList();
       // Collections.sort(shopAddressInfos,comparatorList);
        shopAddressInfos.sort(Comparator.comparing(ShopAddressInfo::getFrontTow));
        for(ShopAddressInfo shopAddressIn:shopAddressInfos){

            System.out.println(shopAddressIn.toString());
        }
        ExcelExportUtil.exportToFile("D:\\我的文档\\a88.xlsx",shopAddressInfos);

    }


    /**
     * 得到中文首字母
     *
     * @param str
     * @return
     */
    public static String getPinYinHeadChar(String str) {
        int tmp= 2;
        if(str.length() < 2){
            tmp = str.length();
        }
        String convert = "";
        for (int j = 0; j < tmp; j++) {
            char word = str.charAt(j);
            String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
            if (pinyinArray != null) {
                convert += pinyinArray[0].charAt(0);
            } else {
                convert += word;
            }
        }
        return convert.toUpperCase();
    }
}
