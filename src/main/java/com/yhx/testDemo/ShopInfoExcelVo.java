package com.yhx.testDemo;


/**
 * @author: mishuai
 * @date: 2020/11/13 13:35
 * @description:
 */

public class ShopInfoExcelVo {




        //(name = "shopId")
        private String shopId;
        //(name = "companyId")
        private String companyId;


        //(name = "shopName")
        private String shopName;
        //(name = "shopSubName")
        private String 	shopSubName;

        //(name = "takeOut")
        private String takeOut;
        //(name = "logo")
        private String logo;
    //(name = " shopAdress")
    private String  shopAdress;

    //(name = "lng")
    private String lng;
    //(name = "lat")
    private String 	lat;




    //(name = "classifyId")
    private String classifyId;
    //(name = "sellLastDate")
    private String sellLastDate;
    //(name = "shopMobile")
    private String shopMobile;

    //(name = "takeOutCode")
    private String takeOutCode;









        //(name = "startSellTime")
        private String 	startSellTime;
        //(name = "endSellTime")
        private String endSellTime;


        //(name = "unionPayCode")
        private String 	unionPayCode;


    public ShopInfoExcelVo() {
    }

    public ShopInfoExcelVo(String shopId, String companyId, String shopName, String shopSubName, String takeOut, String logo, String shopAdress, String lng, String lat, String classifyId, String sellLastDate, String shopMobile, String takeOutCode, String startSellTime, String endSellTime, String unionPayCode) {
        this.shopId = shopId;
        this.companyId = companyId;
        this.shopName = shopName;
        this.shopSubName = shopSubName;
        this.takeOut = takeOut;
        this.logo = logo;
        this.shopAdress = shopAdress;
        this.lng = lng;
        this.lat = lat;
        this.classifyId = classifyId;
        this.sellLastDate = sellLastDate;
        this.shopMobile = shopMobile;
        this.takeOutCode = takeOutCode;
        this.startSellTime = startSellTime;
        this.endSellTime = endSellTime;
        this.unionPayCode = unionPayCode;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopSubName() {
        return shopSubName;
    }

    public void setShopSubName(String shopSubName) {
        this.shopSubName = shopSubName;
    }

    public String getTakeOut() {
        return takeOut;
    }

    public void setTakeOut(String takeOut) {
        this.takeOut = takeOut;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getShopAdress() {
        return shopAdress;
    }

    public void setShopAdress(String shopAdress) {
        this.shopAdress = shopAdress;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getClassifyId() {
        return classifyId;
    }

    public void setClassifyId(String classifyId) {
        this.classifyId = classifyId;
    }

    public String getSellLastDate() {
        return sellLastDate;
    }

    public void setSellLastDate(String sellLastDate) {
        this.sellLastDate = sellLastDate;
    }

    public String getShopMobile() {
        return shopMobile;
    }

    public void setShopMobile(String shopMobile) {
        this.shopMobile = shopMobile;
    }

    public String getTakeOutCode() {
        return takeOutCode;
    }

    public void setTakeOutCode(String takeOutCode) {
        this.takeOutCode = takeOutCode;
    }

    public String getStartSellTime() {
        return startSellTime;
    }

    public void setStartSellTime(String startSellTime) {
        this.startSellTime = startSellTime;
    }

    public String getEndSellTime() {
        return endSellTime;
    }

    public void setEndSellTime(String endSellTime) {
        this.endSellTime = endSellTime;
    }

    public String getUnionPayCode() {
        return unionPayCode;
    }

    public void setUnionPayCode(String unionPayCode) {
        this.unionPayCode = unionPayCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"shopId\":\"")
                .append(shopId).append('\"');
        sb.append(",\"companyId\":\"")
                .append(companyId).append('\"');
        sb.append(",\"shopName\":\"")
                .append(shopName).append('\"');
        sb.append(",\"shopSubName\":\"")
                .append(shopSubName).append('\"');
        sb.append(",\"takeOut\":\"")
                .append(takeOut).append('\"');
        sb.append(",\"logo\":\"")
                .append(logo).append('\"');
        sb.append(",\"shopAdress\":\"")
                .append(shopAdress).append('\"');
        sb.append(",\"lng\":\"")
                .append(lng).append('\"');
        sb.append(",\"lat\":\"")
                .append(lat).append('\"');
        sb.append(",\"classifyId\":\"")
                .append(classifyId).append('\"');
        sb.append(",\"sellLastDate\":\"")
                .append(sellLastDate).append('\"');
        sb.append(",\"shopMobile\":\"")
                .append(shopMobile).append('\"');
        sb.append(",\"takeOutCode\":\"")
                .append(takeOutCode).append('\"');
        sb.append(",\"startSellTime\":\"")
                .append(startSellTime).append('\"');
        sb.append(",\"endSellTime\":\"")
                .append(endSellTime).append('\"');
        sb.append(",\"unionPayCode\":\"")
                .append(unionPayCode).append('\"');
        sb.append('}');
        return sb.toString();
    }
}
